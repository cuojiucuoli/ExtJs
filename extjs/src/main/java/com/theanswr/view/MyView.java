package com.theanswr.view;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

import com.alibaba.fastjson.JSON;
import com.theanswr.bean.BigCity;
import com.theanswr.bean.City;
import com.theanswr.bean.CityList;
import com.theanswr.dao.MyDao;

@Controller
public class MyView {

	@Autowired
	MyDao dao;
	
	@RequestMapping(value="/City.do",produces="text/html;charset=UTF-8")
	@ResponseBody
	public String MyCity() {
		List<City> findAll = dao.findAll();
		Map<String,Object> map = new HashMap();
		map.put("data", findAll);
		map.put("totalCount", findAll.size());
		String jsonString = JSON.toJSONString(map);
		return jsonString;
		
	}
	
	@RequestMapping(value="/EditCityInfo.do",produces="text/html;charset=UTF-8")
	@ResponseBody
	public String EditCityInfo(City city) {
		String cityKey = city.getCityKey();
		City c = dao.findOne(cityKey);
		
		String jsonString = JSON.toJSONString(c);
		return jsonString;
	}
	
	@RequestMapping(value="/ModifyCityInfo.do",produces="text/html;charset=UTF-8")
	@ResponseBody
	public String modifyCityInfo(City city) {
		
			try {
				dao.saveAndFlush(city);
				return "success";
			} catch (Exception e) {
				return "failed";
			}
		
	}
	
	
	@RequestMapping(value="/DeleteCityInfo.do",produces="text/html;charset=UTF-8")
	@ResponseBody
	public String deleteCityInfo(String ids) {
		
		return null;
	}
}
