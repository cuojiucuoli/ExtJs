package com.theanswr.bean;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(name="City")
public class City {
	

	@Id
	private String CityKey;
	@Column
	private String CityValue;
	public String getCityKey() {
		return CityKey;
	}
	public void setCityKey(String cityKey) {
		CityKey = cityKey;
	}
	public String getCityValue() {
		return CityValue;
	}
	public void setCityValue(String cityValue) {
		CityValue = cityValue;
	}
	
	
}
