package com.theanswr.bean;

import java.util.List;

public class CityList {

	List<City> city;

	public List<City> getCity() {
		return city;
	}

	public void setCity(List<City> city) {
		this.city = city;
	}
	
}
