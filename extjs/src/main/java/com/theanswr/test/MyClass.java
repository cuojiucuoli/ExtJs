package com.theanswr.test;

import java.util.List;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Configuration;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

import com.theanswr.bean.City;
import com.theanswr.dao.MyDao;
@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration("classpath:spring/applicationContext.xml")
public class MyClass {

	@Autowired
	MyDao dao;
	@Test
	public void My() {
		City city = new City();
		city.setCityKey("1");
		city.setCityValue("中国2");
		
		dao.save(city);
	}
}
