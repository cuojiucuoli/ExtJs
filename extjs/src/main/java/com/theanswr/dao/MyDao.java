package com.theanswr.dao;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;

import com.theanswr.bean.City;

public interface MyDao extends JpaRepository<City, String>{
	@Query(value="FROM City c Where  c.CityValue = ?1")
	List<City> selectCity(String value);
	@Query(value="DELETE CITY c WHERE c.CITYVALUE IN ")
	public void cancelCityByIds(String ids);
}
