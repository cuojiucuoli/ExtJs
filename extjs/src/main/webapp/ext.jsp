<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
	pageEncoding="ISO-8859-1"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
<title>Insert title here</title>
<link rel="stylesheet" type="text/css"
	href="./extjs/resources/css/ext-all.css">
<script type="text/javascript" src="./extjs/ext-all.js"></script>


<script type="text/javascript">
	Ext.onReady(function() {

		Ext.define('MyModel', {
			extend : 'Ext.data.Model',
			fields : [ {
				name : 'cityKey'
			}, {
				name : 'cityValue'
			} ]
		})

		var states = Ext.create('Ext.data.Store', {
			model : 'MyModel',
			proxy : {
				type : 'ajax',
				url : 'City.do'
			},
			autoLoad : true
		});
		Ext.create('Ext.form.Panel', {
			title : 'register',
			bodyPadding : 10,
			width : 500,
			height : 500,
			bodyPadding: 10,
			// The form will submit an AJAX request to this URL when submitted
			url : 'save-form.php',

			// Fields will be arranged vertically, stretched to full width
			layout : 'anchor',
			defaults : {
				anchor : '100%'
			},

			// The fields
			defaultType : 'textfield',
			items : [ {
				fieldLabel : 'username',
				blankText : 'is not null',
				allowBlank : false
			}, {
				fieldLabel : 'password',
				allowBlank : false
			}, new Ext.form.field.Number({
				fieldLabel : 'age'
			}), {
				xtype : 'radiogroup',
				fieldLabel : 'sex',
				items : [ {
					boxLabel : 'boy',
					inputValue : 'boy',
				}, {
					boxLabel : 'girl',
					inputValue : 'girl'
				} ]
			}, {
				xtype : 'combo',
				fieldLabel : 'Choose State',
				store : states,
				queryMode : 'local',
				displayField : 'cityKey',
				valueField : 'cityValue'

			} ,{
		        xtype: 'datefield',
		        name: 'logintime',
		        fieldLabel: 'logintime',
		        value: new Date(),
		        format: 'Y-m-d'
		    }],
			// Reset and Submit buttons
			buttons : [ {
				text : 'Reset',
				handler : function() {
					this.up('form').getForm().reset();
				}
			}, {
				text : 'Submit',
				formBind : true, //only enabled once the form is valid
				disabled : true,
				handler : function() {
					var form = this.up('form').getForm();
					if (form.isValid()) {
						form.submit({
							success : function(form, action) {
								Ext.Msg.alert('Success', action.result.msg);
							},
							failure : function(form, action) {
								Ext.Msg.alert('Failed', action.result.msg);
							}
						});
					}
				}
			}

			],
			renderTo : panel
		});
	})
</script>
</head>
<body>
	<div id="panel"></div>

</body>
</html>