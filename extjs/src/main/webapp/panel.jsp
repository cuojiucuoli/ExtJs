<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
	pageEncoding="ISO-8859-1"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
<title>Insert title here</title>

<link rel="stylesheet" type="text/css"
	href="./extjs/resources/css/ext-all.css">
<script type="text/javascript" src="./extjs/ext-all.js"></script>


<script type="text/javascript">
		Ext.onReady(function(){
			Ext.create('Ext.panel.Panel',{
				width: 200,
			    height: 200,
			    renderTo: 'panel',
			    title: 'register',
			    tools: [{
			    		type: 'help',
			    		callback: function(){
				    		Ext.MessageBox.alert("help you!");
				    	}
				    		
			    	},{
			    		itemId: 'refresh',
			    		type: 'refresh'
			    	},{
			    		itemId: 'search',
			    		type: 'search'
			    	}],loader:{
			    		url: 'City.do',
			    		autoLoad: true
			    	}
			})
		})
</script>
</head>
<body>
	<div id="panel"></div>

</body>
</html>