<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<title>Insert title here</title>
<link rel="stylesheet" type="text/css"
	href="./extjs/resources/css/ext-all.css">
<script type="text/javascript" src="./extjs/ext-all.js"></script>


<script type="text/javascript">
	Ext.onReady(
	
		function(){
			var store1 =	Ext.create('Ext.data.TreeStore', {
     		    root: {
     		        expanded: true,
     		        children: [
     		            { text: "修改用户", leaf: true },
     		            { text: "查询用户", expanded: true, children: [
     		                { text: "查询指定条件用户", leaf: true },
     		                { text: "查询所有用户", leaf: true}
     		            ] },
     		            { text: "删除用户", leaf: true },
     		            { text: "新增用户", leaf: true }
     		        ]
     		    }
     		})	
			
			
			Ext.create('Ext.panel.Panel',{
				 title: 'Results',
				 width: 600,
				 height: 400,
				 renderTo: Ext.getBody(),
				 layout: {
				        type: 'accordion',
				        titleCollapse: false,
				        animate: true
				 }, items: [
					 {
				        title: 'Panel 1',
				        items: [
				        	Ext.create('Ext.tree.Panel', {
				        		border: 0,
				        	    store: store1,
				        	    rootVisible: false,
				        	    renderTo: Ext.getBody()
				        	})
				        ]
				    },{title: 'Panel 2',
				    	items: [Ext.create('Ext.tree.Panel', {
					        title: 'Simple Tree',
					        root: {
					            text: "Root node",
					            expanded: true,
					            children: [
					                { text: "Child 1", leaf: true },
					                { text: "Child 2", leaf: true }
					            ]
					        },
					        renderTo: Ext.getBody()
					    })]},{
				        title: 'Panel 3',
				        html: 'Panel content!'
				    }]
			})
		}		
	
	)
		
</script>
</head>
<body>
	<div id="panel"></div>

</body>
</html>