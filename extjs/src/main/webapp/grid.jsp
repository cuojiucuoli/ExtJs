<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<title>Insert title here</title>
<link rel="stylesheet" type="text/css"
	href="./extjs/resources/css/ext-all.css">
<link rel="stylesheet" type="text/css" href="./css/define.css">
<script type="text/javascript" src="./extjs/ext-all.js"></script>
<script type="text/javascript">
	
 
	
	Ext.onReady(function(){
		
		 var mystore=Ext.create('Ext.data.Store',{
				fields:['cityKey','cityValue'],
				proxy:{
					type:"ajax",
					url:"City.do",
					reader:{
						type:"json",
						root:"data",
						totalProperty:"totalCount"  //可有可无 
 					}
				},
				autoLoad:true
			})
		var panel=Ext.create('Ext.grid.Panel',{
			title: 'Simpsons',
			frame:true,
			height:400,
			renderTo: Ext.getBody(),
			columns:[
				{
					text:"城市名 ",
					dataIndex:"cityKey"
				},{
					text:"城市别名",
					dataIndex:"cityValue"
				},{
					xtype: "actioncolumn",
					text:"操作",
					width:50,
					items:[{
						icon:"./extjs/resources/ext-theme-classic-sandbox/images/shared/icon-error.gif",
						tooltip:"删除 "
		            },{	
		            	icon:"./extjs/resources/ext-theme-classic-sandbox/images/dd/drop-add.gif",
		            	tooltip:"修改 "
		            }]
				}
			],	
			store: mystore,
			dockedItems:[
				{
					xtype:"pagingtoolbar",
					store: mystore,
					dock: 'bottom',
					displayInfo:true,
					displayMsg :"当前显示从第{0}到{1},总共{2}条",
					emptyMsg:"无记录",
					displayInfo: true
				}
			]
		
		})
	})
</script>
</head>
<body>
	<div id="panel"></div>

</body>
</html>