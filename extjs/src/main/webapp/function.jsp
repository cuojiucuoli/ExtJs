<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<title>Insert title here</title>
<link rel="stylesheet" type="text/css"
	href="./extjs/resources/css/ext-all.css">
<script type="text/javascript" src="./extjs/ext-all.js"></script>


<script type="text/javascript">
Ext.onReady(function(){
	new Ext.window.Window({
		titled:"111",
		layout: "form",
		items:[{
			xtype:"textfield",
			id:"text",
			fieldLabel:"用户名"
		},{
			xtype:"button",
			text:"提交",
			handler: function(){
				var text = Ext.getCmp("text").value;
				Ext.Ajax.request({
				    url: 'City.do',
				    params: {"city[0].CityValue": text},
				    success: function(response){
				        var text = response.responseText;
				        var json = Ext.JSON.decode(text);
				    	alert(json[0].cityKey);
				    }
				});
			}
		}]
	}).show();
})
 
</script>
</head>
<body>
	<div id="panel"></div>

</body>
</html>