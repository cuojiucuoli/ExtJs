﻿Ext.define("Ext.com.itcast", {
	extend : "Ext.grid.Panel",
	id : "panels",
	title : '查看学生信息',
	frame : true,
	height : 400,
	closeAction:'hide',
	initComponent : function() {
		
		var modifyPanel = new Ext.form.Panel({
						id: "form",
					frame : true,
					closable:false,
					closeAction:"hide",
					defaultType : 'textfield',
					items : [{
								fieldLabel : '城市名',
								   id: 'CityKey',
								name : 'first',
								allowBlank : false
							}, {
								fieldLabel : '城市别名',
								  id :'CityValue',
								name : 'last',
								allowBlank : false
							}],
					buttons : [{
								text : '取消',
								handler : function() {
									this.up('form').getForm().reset();
									Ext.getCmp("window").hide();
								}
							},{
								text:'提交',
								handler:function(){
								var cityValue =Ext.getCmp("CityValue").getValue();	
								var cityKey =Ext.getCmp("CityKey").getValue();						
									Ext.Ajax.request({
										url:"ModifyCityInfo.do",
										params:{
											CityKey:cityKey,
											CityValue:cityValue
										},
										success:function(response){										
											if(response.responseText=="success"){
												Ext.getCmp("window").hide();												
												mystore.reload();
											}else if(response.responseText=="failed"){
												Ext.MessageBox.alert("修改失败，请重新修改");
											}
										}
									})
								}
							}]

				})

		var mystore = Ext.create('Ext.data.Store', {
					fields : ['cityKey', 'cityValue'],
					proxy : {
						type : "ajax",
						url : "City.do",
						reader : {
							type : "json",
							root : "data",
							totalProperty : "totalCount"
						}
					},
					autoLoad : true
				});

		Ext.apply(this, {
			closable : true,
			store : mystore,
			forceFit : true, // 表格grid自适应
			selType:"checkboxmodel",
			multiSelect:true,
			columns : [
					  {	width:200,
					  	xtype:"rownumberer",
					  	text:"编号",
					  	render:function(value,cellmeta,record,rowIndex,columnIndex,store){
					  		return rowIndex+1;
					  	}
					},{
						text : "城市名 ",
						dataIndex : "cityKey"
					}, {
						text : "城市别名",
						dataIndex : "cityValue"
					}, {
						xtype : "actioncolumn",
						text : "操作",
						width : 50,
						items : [{
							icon : "./extjs/resources/ext-theme-classic-sandbox/images/shared/icon-error.gif",
							tooltip : "删除 ",
							handler : function() {
								
							}
						}, {
							icon : "./extjs/resources/ext-theme-classic-sandbox/images/dd/drop-add.gif",
							tooltip : "修改 ",
							handler : function(grid, lines, rows) {
								var data = grid.getStore().getAt(lines);
								var cityKey = data.get("cityKey");
								var cityValue = data.get("cityValue");						
								Ext.Ajax.request({
									url:"EditCityInfo.do",
									params:{
										cityKey:cityKey
									},success:function(response){
										var text = response.responseText;
										var json = Ext.decode(text);
										Ext.getCmp("CityKey").setValue(json.cityKey);
										Ext.getCmp("CityValue").setValue(json.cityValue);
									}
								})
							
								Ext.create('Ext.window.Window', {
											id:"window",
											title : '修改學生信息',
											height : 200,
											width : 400,
											layout : 'fit',
											items : modifyPanel,
											closeAction:"hide",
											items:[modifyPanel]
										}).show();
								
							}
						}]
					}],
			dockedItems : [{
						xtype : "pagingtoolbar",
						store : mystore,
						dock : 'bottom',
						displayInfo : true,
						displayMsg : "当前显示从第{0}到{1},总共{2}条",
						emptyMsg : "无记录"
					}, {
						xtype : "toolbar",
						dock : "top",
						items : [{
							xtype : "button",
							text : "修改學生信息",
							handler : function() {
								Ext.create('Ext.window.Window', {
											title : '修改學生信息',
											height : 200,
											width : 400,
											layout : 'fit',
											items : { // Let's put an empty
												// grid in
												// just to illustrate fit layout
												xtype : 'grid',
												forceFit : true,
												border : false,
												columns : [{
															text : '城市名',
															dataIndex : 'cityKey'
														}, {
															text : '城市別名',
															dataIndex : 'cityValue'
														}], // One header just
												// for show.
												// There's no data,
												store : mystore
												// A dummy empty data store
											}
										}).show();
							}
						}, {
							xtype : "button",
							text : "添加學生信息"
						}, {
							xtype : "button",
							text : "刪除學生信息",
							handler:function(){
							var recs = Ext.getCmp("panels").getSelectionModel().getSelection();
								var ids="";	
								for(var i=0;i<recs.length;i++){
									var id	= recs[i].get("cityKey");
									  ids += id + ',';
								}
								
								var	keys =ids.substring(0,ids.length-1);
								alert(keys);
								Ext.Ajax.request({
									url:"DeleteCityInfo.do",
									params:{
										ids:keys
									},
									success:function(response){
										var text = response.responseText;
									}
								})
							}
						}]
					}]
		})
		this.callParent(arguments);
	}

})