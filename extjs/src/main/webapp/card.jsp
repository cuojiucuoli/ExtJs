<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<title>Insert title here</title>
<link rel="stylesheet" type="text/css"
	href="./extjs/resources/css/ext-all.css">
<script type="text/javascript" src="./extjs/ext-all.js"></script>


<script type="text/javascript">
	Ext.onReady(function() {
		var p = Ext.create('Ext.panel.Panel', {
			tbar : [ {
				width : '800',
				height : '800'
			} ],
			layout : 'card',
			items : [ {
				html : 'Card 1',
				id : 's0',
				title : '面板1'
			}, {
				html : 'Card 2',
				id : 's1',
				title : '面板2'
			}, {
				html : 'Card 3',
				id : 's2',
				title : '面板3'
			} ],
			bbar : [ {
				xtype : 'button',
				id : '-1',
				text : '上一页',
				handler : function(obj) {
					var last = obj.id;

					var layout = p.layout;
					var id = parseInt(layout.activeItem.id.substring(1));
					var activeId = id + parseInt(obj.id);
					alert(activeId);
					index = 's'+activeId;
					layout.setActiveItem(index);
				}
			}, {
				xtype : 'button',
				text : '下一页',
				id: '1',
				handler : function(obj) {
					var last = obj.id;

					var layout = p.layout;
					var id = parseInt(layout.activeItem.id.substring(1));
					var activeId = id + parseInt(obj.id);
					alert(activeId);
					p.layout.setActiveItem(activeId);
				}
			} ],
			renderTo : Ext.getBody()
		});
	})
</script>
</head>
<body>
	<div id="panel"></div>

</body>
</html>