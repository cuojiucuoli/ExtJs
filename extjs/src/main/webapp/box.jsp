<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<title>Insert title here</title>
<link rel="stylesheet" type="text/css"
	href="./extjs/resources/css/ext-all.css">
<link rel="stylesheet" type="text/css" href="./css/define.css">
<script type="text/javascript" src="./extjs/ext-all.js"></script>
<script type="text/javascript" src="./jscrip/mygrid.js" charset="UTF-8"></script>
<script type="text/javascript">
	Ext.onReady(function() {
				var view = new Ext.Viewport(
						{	
							layout : "border",
							id:"view",
							frame: true,
							items : [
									{
										region : "north",
										title : "欢迎来到选课系统",
										bodyStyle : "background-image: url(./image/zhu.jpg);background-repeat:no-repeat;width:100%;height:auto;display:block;",
										height : 200
									},
									{
										region : "south",
										title : "欢迎XX同学",
										height : 100
									},
									{
										region : "west",
										title : "在线选课系统菜单 ",
										id:"head",
										width : 200,
									    items:[																
											Ext.create('Ext.tree.Panel',{
												listeners:{
													itemclick:{
														fn:function(value,cm,cs,index){
															if(cm.get("text")=="查看学生信息"){
																var panel = new Ext.com.itcast(); 								
																var center =Ext.getCmp("center");															
																center.add({
																	closable:true,
																	closeAction:"hide",
																	id:"searchInfo",
																	title:"用戶管理",
																	xtype:"tabpanel",																
																	items:[panel]
																});
																center.setActiveTab(Ext.getCmp("searchInfo"));
															}if(cm.get("text")=="修改学生信息"){
																
															}if(cm.get("text")=="添加学生信息"){
																
															}if(cm.get("text")=="添加学生信息"){
																
															}
														}
													}
												},
												frame: true,
										        root: {	
										        	text:"系统菜单",
										        	id:"root",
										            expanded: true,									            
										            children: [
										                {	
										                	text:"用户管理",	
										                	id:"um",
										                	children:[
										                		{ text: "查看学生信息", leaf: true },
												                { text: "修改学生信息", leaf: true },
												                { text: "添加学生信息", leaf: true },
												                { text: "添加学生信息", leaf: true }
										                	]
										                },{
										                	text:"成绩管理",										                	
										                	children:[
										                		{ text: "查看成绩信息", leaf: true },
												                { text: "修改成绩信息", leaf: true },
												                { text: "添加成绩信息", leaf: true },
												                { text: "添加成绩信息", leaf: true }
										                	]
										                },{
										                	text:"课程管理",										                	
										                	children:[
										                		{ text: "查看课程信息", leaf: true },
												                { text: "修改课程信息", leaf: true },
												                { text: "增加课程信息", leaf: true },
												                { text: "删除课程信息", leaf: true }
										                	]
										                },{
										                	text:"报名管理",
										                	children:[
										                		{ text: "查看报名信息", leaf: true },
												                { text: "修改报名信息", leaf: true },
												                { text: "增加报名信息", leaf: true },
												                { text: "删除报名信息", leaf: true }
										                	]
										                }
										            ]
										        },
										        renderTo: Ext.getBody()
										})
									]

									}, {
										region : "center",
										id:"center",
										xtype: "tabpanel",
										items: [{
											title: "首页",
											bodyStyle: "background-image: url(./image/111.jpg);background-repeat:no-repeat;width:auto;height:auto;display:block;"
										}]
									} ]
						})
			})
</script>
</head>
<body>
	<div id="panel"></div>

</body>
</html>